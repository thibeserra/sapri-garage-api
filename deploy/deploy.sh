#!/bin/bash

curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
sudo ./aws/install --update -i /usr/local/aws-cli -b /usr/local/bin
mvn clean package -DskipTests
cp docker-compose.yml compose.yml
docker-compose -f compose.yml build
aws ecr get-login-password --region ${AWS_DEFAULT_REGION} | docker login --username AWS --password-stdin ${IMAGE_REPO_URL}
docker tag sapri-garage-api:latest ${IMAGE_REPO_URL}:latest
docker push ${IMAGE_REPO_URL}:latest
aws ecs update-service --cluster ${CLUSTER_NAME} --task-definition ${TASK_NAME} --service ${SERVICE_NAME} --force-new-deployment
