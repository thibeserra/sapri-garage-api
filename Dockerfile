FROM maven:latest
MAINTAINER Thiago Beserra
ENV TZ=America/Sao_Paulo
ENV PORT=8081
ENV APP_PATH=/garage/api
ENV PROFILE=uat
ENV DB_URL=jdbc:mysql://192.168.99.106:3306/garage
ENV DB_USERNAME=usr-garage
ENV DB_PASSWORD=gara
COPY . /var/www
WORKDIR /var/www
RUN mvn clean package -DskipTests
#ADD /var/www/target/sapri-garage-api-1.0.0.jar /sapri-garage-api-1.0.0.jar
RUN mv target/sapri-garage-api-1.0.0.jar /sapri-garage-api-1.0.0.jar
EXPOSE 8081
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom", "-jar", "/sapri-garage-api-1.0.0.jar"]