package com.sapri.garage.test.repository;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.time.LocalDateTime;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.sapri.garage.model.Brand;
import com.sapri.garage.repository.BrandRepository;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@DataJpaTest
public class BrandRepositoryTests {

	@Autowired
	private BrandRepository brandRepository;
	
	@Test
	public void whenInjectDependenciesReturnNotNull() {
		assertNotNull(this.brandRepository);
	}
	
	@Test
	public void whenSaveBrandPersistWithSucess() {
		Brand brand = Brand
						.builder()
							.createDate(LocalDateTime.now())
							.description("Fiat")
						.build();
		
		brand = this.brandRepository.save(brand);
		
		assertNotNull(brand.getId());
		assertNotNull(brand.getCreateDate());
		assertNull(brand.getUpdateDate());
		assertNull(brand.getExtId());
		assertNotNull(brand.getDescription());
	}
	
}
