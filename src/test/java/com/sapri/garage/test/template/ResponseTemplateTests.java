package com.sapri.garage.test.template;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.net.UnknownHostException;
import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.sapri.garage.dto.BrandDto;
import com.sapri.garage.dto.ResponseBodyDTO;
import com.sapri.garage.error.ErrorDtoFactory;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
public class ResponseTemplateTests {

	@Test
	public void whenCreateResponseTemplateThenAddRecordTwoRecords() throws UnknownHostException {
		ResponseBodyDTO<BrandDto> response = new ResponseBodyDTO<>();
		response.addRecord(BrandDto.builder().description("fiat").build());
		response.addRecord(BrandDto.builder().description("ford").build());
		response.setErrors(new ArrayList<>());
		
		assertNotNull(response.getMeta());
		assertNotNull(response.getMeta().getServer());
		assertEquals(0, response.getMeta().getOffset());
		assertEquals(2, response.getMeta().getRecordCount());
		assertNotNull(response.getRecords());
		assertEquals(2, response.getRecords().size());
		assertTrue(response.isSucess());
		
	}
	
	@Test
	public void whenCreateResponseTemplateThenAddTwoErrors() throws UnknownHostException {
		ResponseBodyDTO<BrandDto> response = new ResponseBodyDTO<>();
		response.addError(ErrorDtoFactory.getInternalServerError("fail 1"));
		response.addError(ErrorDtoFactory.getInternalServerError("fail 2"));
		
		assertNotNull(response.getMeta());
		assertNotNull(response.getMeta().getServer());
		assertEquals(0, response.getMeta().getLimit());
		assertEquals(0, response.getMeta().getOffset());
		assertEquals(0, response.getMeta().getRecordCount());
		assertNull(response.getRecords());
		assertNotNull(response.getErrors());
		assertEquals(2, response.getErrors().size());
		assertFalse(response.isSucess());
	}
	
	
}
