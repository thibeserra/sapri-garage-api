package com.sapri.garage.test.service;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.sapri.garage.dto.BrandDto;
import com.sapri.garage.dto.ResponseBodyDTO;
import com.sapri.garage.error.ErrorDTO;
import com.sapri.garage.model.Brand;
import com.sapri.garage.repository.BrandRepository;
import com.sapri.garage.service.BrandServiceImpl;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
public class BrandServiceTests {

	@InjectMocks
	private BrandServiceImpl brandService;
	
	@Mock
	private BrandRepository brandRepository;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void whenInjectDependenciesThenReturn() {
		assertNotNull(this.brandService);
		assertNotNull(this.brandRepository);
	}
	
	@Test
	public void whenSaveBrandThenReturnSucess() throws UnknownHostException {
		doReturn(new Brand()).when(this.brandRepository).save(any(Brand.class));
		ResponseBodyDTO response = this.brandService.save(new BrandDto());
		assertNotNull(response);
		assertNotNull(response.getMeta());
		assertNull(response.getErrors());
		assertTrue(response.isSucess());
	}
	
	@Test
	public void whenSaveBrandThenReturnError() throws UnknownHostException {
		doThrow(RuntimeException.class).when(this.brandRepository).save(any(Brand.class));
		ResponseBodyDTO response = this.brandService.save(new BrandDto());
		assertNotNull(response);
		assertNotNull(response.getMeta());
		assertNotNull(response.getErrors());
		assertFalse(response.isSucess());
		assertEquals(10000L, ((ErrorDTO) response.getErrors().get(0)).getErrorCode());
	}
	
	@Test
	public void whenFindAllBrandsThenReturnSucess() throws UnknownHostException {
		
		List<Brand> brands = new ArrayList<>();
		brands.add(Brand.builder().id(1L).build());
		
		doReturn(brands).when(this.brandRepository).findAll();
		ResponseBodyDTO response = this.brandService.findAll();
		assertNotNull(response);
		assertNotNull(response.getMeta());
		assertNull(response.getErrors());
		assertTrue(response.isSucess());
	}
	
	@Test
	public void whenFindAllBrandsThenReturnError() throws UnknownHostException {		
		
		doThrow(RuntimeException.class).when(this.brandRepository).findAll();
		ResponseBodyDTO response = this.brandService.findAll();
		assertNotNull(response);
		assertNotNull(response.getMeta());
		assertNotNull(response.getErrors());
		assertFalse(response.isSucess());
		assertEquals(10000L, ((ErrorDTO) response.getErrors().get(0)).getErrorCode());
		
		
	}
	
	@Test
	public void whenRemoveBrandThenReturnSucess() throws UnknownHostException {
		doNothing().when(this.brandRepository).deleteById(any(Long.class));
		ResponseBodyDTO response = this.brandService.remove(any(Long.class));
		assertNotNull(response);
		assertNotNull(response.getMeta());
		assertNull(response.getErrors());
		assertTrue(response.isSucess());
	}
	
	@Test
	public void whenRemoveBrandThenReturnError() throws UnknownHostException {
		doThrow(RuntimeException.class).when(this.brandRepository).deleteById(any(Long.class));
		ResponseBodyDTO response = this.brandService.remove(any(Long.class));
		assertNotNull(response);
		assertNotNull(response.getMeta());
		assertNotNull(response.getErrors());
		assertFalse(response.isSucess());
		assertEquals(10000L, ((ErrorDTO) response.getErrors().get(0)).getErrorCode());
	}
		
}
