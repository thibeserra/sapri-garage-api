package com.sapri.garage.test.controller;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;

import java.net.UnknownHostException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.sapri.garage.controller.BrandController;
import com.sapri.garage.dto.BrandDto;
import com.sapri.garage.dto.ResponseBodyDTO;
import com.sapri.garage.error.ErrorDtoFactory;
import com.sapri.garage.service.BrandService;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")	
public class BrandControllerTests {

	@InjectMocks
	private BrandController brandController;
	
	@Mock
	private BrandService brandService;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void whenInjectDependenciesReturnNotNull() {
		assertNotNull(this.brandController);
		assertNotNull(this.brandService);
	}
	
	@Test
	public void whenCreateBrandReturnCreatedStatus() throws UnknownHostException {
		
		doReturn(new ResponseBodyDTO()).when(this.brandService).save(any(BrandDto.class));
		
		ResponseEntity<?> response = this.brandController.save(BrandDto.builder().description("Fiat").build());
		
		assertNotNull(response);
		assertEquals(HttpStatus.CREATED, response.getStatusCode());
		
		
	}
	
	@Test
	public void whenCreateBrandReturnInternalServerErrorStatus() throws UnknownHostException {
		ResponseBodyDTO responseTemplate = new ResponseBodyDTO<>();
		responseTemplate.addError(ErrorDtoFactory.getInternalServerError("fail"));
		doReturn(responseTemplate).when(this.brandService).save(any(BrandDto.class));
		
		ResponseEntity<?> response = this.brandController.save(BrandDto.builder().description("Fiat").build());
		assertNotNull(response);
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
	}
	
	@Test
	public void whenListBrandsThenReturnOkStatus() {
		doReturn(new ResponseBodyDTO()).when(this.brandService).findAll();
		ResponseEntity<?> response = this.brandController.list();
		assertNotNull(response);
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}
	
	@Test
	public void whenListBrandsThenReturnInternalServerErrorStatus() {
		ResponseBodyDTO responseTemplate = new ResponseBodyDTO();
		responseTemplate.addError(ErrorDtoFactory.getInternalServerError("fail"));
		doReturn(responseTemplate).when(this.brandService).findAll();
		ResponseEntity<?> response = this.brandController.list();
		assertNotNull(response);
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
	}
	
	@Test
	public void whenRemoveBrandThenReturnNoContentStatus() {
		doReturn(new ResponseBodyDTO()).when(this.brandService).remove(any(Long.class));
		
		ResponseEntity<?> response = this.brandController.remove(any(Long.class));
		
		assertNotNull(response);
		assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
	}
	
	@Test
	public void whenRemoveBrandThenReturnInternalServerStatus() {
		ResponseBodyDTO responseTemplate = new ResponseBodyDTO();
		responseTemplate.addError(ErrorDtoFactory.getInternalServerError("fail"));
		doReturn(responseTemplate).when(this.brandService).remove(any(Long.class));
		ResponseEntity<?> response = this.brandController.remove(any(Long.class));
		assertNotNull(response);
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
	}
	
	
	
	
}
