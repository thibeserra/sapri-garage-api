package com.sapri.garage.test.transform;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.sapri.garage.dto.BrandDto;
import com.sapri.garage.model.Brand;
import com.sapri.garage.transform.BrandTransform;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
public class BrandTransformTests {

	@Test
	public void whenTransformBrandToDtoReturnSucess() {
		
		Brand brand = Brand
						.builder()
							.description("Fiat")
						.build();
		
		BrandDto brandDto = BrandTransform.toDto(brand);
		
		assertNull(brandDto.getId());
		assertNull(brandDto.getExtId());
		assertNotNull(brandDto.getDescription());
		
	}
	
	@Test
	public void whenTransformBrandDtoToBrandReturnSucess() {
		
		BrandDto brandDto = BrandDto
								.builder()
									.description("Fiat")
								.build();
		
		Brand brand = BrandTransform.toEntity(brandDto);
		
		assertNull(brand.getId());
		assertNotNull(brand.getCreateDate());
		assertNull(brand.getExtId());
		assertNotNull(brand.getDescription());
		
	}
	
}
