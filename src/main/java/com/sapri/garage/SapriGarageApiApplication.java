package com.sapri.garage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"com.sapri.garage"})
public class SapriGarageApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SapriGarageApiApplication.class, args);
	}

}
