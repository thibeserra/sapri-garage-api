package com.sapri.garage.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sapri.garage.dto.BrandDto;
import com.sapri.garage.dto.ResponseBodyDTO;
import com.sapri.garage.error.ErrorDtoFactory;
import com.sapri.garage.model.Brand;
import com.sapri.garage.repository.BrandRepository;
import com.sapri.garage.transform.BrandTransform;

@Service
public class BrandServiceImpl implements BrandService {

	@Autowired
	private BrandRepository brandRepository;
	
	@Override
	public ResponseBodyDTO save(BrandDto brandDTO) {
		
		ResponseBodyDTO response = new ResponseBodyDTO();
		try {
			Brand brandEntity = this.brandRepository.save(BrandTransform.toEntity(brandDTO));
			response.addRecord(BrandTransform.toDto(brandEntity));
		} catch(Exception e) {
			response.addError(ErrorDtoFactory.getInternalServerError(e.getMessage()));
		}		
		
		return response;
	}

	@Override
	public ResponseBodyDTO findAll() {
		ResponseBodyDTO response = new ResponseBodyDTO();
		
		try {
			List<Brand> brandsEntity = this.brandRepository.findAll();
			response.setRecords(brandsEntity);
		} catch(Exception e) {
			response.addError(ErrorDtoFactory.getInternalServerError(e.getMessage()));
		}
		
		return response;
	}

	@Override
	public ResponseBodyDTO remove(Long brandId) {
		
		ResponseBodyDTO response = new ResponseBodyDTO();
		
		try {
			this.brandRepository.deleteById(brandId);
		} catch(Exception e) {
			response.addError(ErrorDtoFactory.getInternalServerError(e.getMessage()));
		}
		
		return response;
		
	}

}
