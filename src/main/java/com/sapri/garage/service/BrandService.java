package com.sapri.garage.service;

import com.sapri.garage.dto.BrandDto;
import com.sapri.garage.dto.ResponseBodyDTO;


public interface BrandService {

	public ResponseBodyDTO save(BrandDto brandDTO);
	public ResponseBodyDTO findAll();
	public ResponseBodyDTO remove(Long brandId);
	
}
