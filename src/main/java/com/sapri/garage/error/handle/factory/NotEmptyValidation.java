package com.sapri.garage.error.handle.factory;


import org.springframework.validation.FieldError;

import com.sapri.garage.error.ErrorDTO;
import com.sapri.garage.error.ErrorDtoFactory;

public class NotEmptyValidation implements HandleValidationType {

	@Override
	public ErrorDTO create(FieldError fieldError) {
		return ErrorDtoFactory.getMissingParameter("value", fieldError.getField());
	}

}
