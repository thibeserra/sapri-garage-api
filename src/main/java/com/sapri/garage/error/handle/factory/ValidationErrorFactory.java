package com.sapri.garage.error.handle.factory;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class ValidationErrorFactory {

    private ValidationErrorFactory() {
    }

    static Map<String, HandleValidationType> operationMap = new HashMap<>();
    static {
        operationMap.put("NotNull", new NotNullValidation());
        operationMap.put("NotEmpty", new NotEmptyValidation());
    }

    public static Optional<HandleValidationType> getOperation(String operator) {
        return Optional.ofNullable(operationMap.get(operator));
    }

}
