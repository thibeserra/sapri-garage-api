package com.sapri.garage.error.handle;

import org.springframework.validation.FieldError;

import com.sapri.garage.error.ErrorDTO;
import com.sapri.garage.error.handle.factory.HandleValidationType;
import com.sapri.garage.error.handle.factory.ValidationErrorFactory;

import java.util.List;
import java.util.stream.Collectors;

public class HandleValidationError {
    public List<ErrorDTO> createErrorsFromList(List<FieldError> fieldErrorList) {
        return fieldErrorList.stream().map(this::createError).collect(Collectors.toList());
    }

    public ErrorDTO createError(FieldError fieldError) {
        HandleValidationType error = ValidationErrorFactory.getOperation(fieldError.getCode())
                .orElseThrow(() -> new IllegalArgumentException("Invalid Error"));

        return error.create(fieldError);
    }
}
