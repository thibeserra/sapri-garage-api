package com.sapri.garage.error.handle.factory;

import org.springframework.validation.FieldError;

import com.sapri.garage.error.ErrorDTO;

public interface HandleValidationType {
    ErrorDTO create(FieldError fieldError);
}
