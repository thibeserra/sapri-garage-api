package com.sapri.garage.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sapri.garage.model.Brand;

public interface BrandRepository extends JpaRepository<Brand, Long> {

}
