package com.sapri.garage.transform;

import java.time.LocalDateTime;

import com.sapri.garage.dto.BrandDto;
import com.sapri.garage.model.Brand;

public class BrandTransform {

	private BrandTransform() {}
	
	public static BrandDto toDto(Brand brand) {
		return BrandDto
				.builder()
					.id(brand.getId())
					.extId(brand.getExtId())
					.description(brand.getDescription())
				.build();
	}
	
	public static Brand toEntity(BrandDto brandDto) {
		return Brand
				.builder()
				.id(brandDto.getId())
				.createDate(LocalDateTime.now())
				.extId(brandDto.getExtId())
				.description(brandDto.getDescription())
			.build();	
	}
	
}
