package com.sapri.garage.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sapri.garage.dto.BrandDto;
import com.sapri.garage.dto.ResponseBodyDTO;
import com.sapri.garage.error.ErrorCodeEnum;
import com.sapri.garage.error.ErrorDTO;
import com.sapri.garage.service.BrandService;

@RestController
@RequestMapping("/brand")
@CrossOrigin
public class BrandController {
	
	@Autowired
	private BrandService brandService;
	
	
	@PostMapping
	public ResponseEntity save(@RequestBody BrandDto brand) {
		
		ResponseBodyDTO response = this.brandService.save(brand);
		
		if (!response.isSucess()) {
			ErrorDTO error = (ErrorDTO) response.getErrors().get(0);
			return new ResponseEntity(response, ErrorCodeEnum.findHttpStatus(error.getErrorCode()));
		}
		
		return new ResponseEntity(response.getRecords(), HttpStatus.CREATED);
		
	}
	
	@GetMapping
	public ResponseEntity list() {
		
		ResponseBodyDTO<?> response = this.brandService.findAll();
		
		if (!response.isSucess()) {
			ErrorDTO error =  response.getErrors().get(0);
			return new ResponseEntity(response, ErrorCodeEnum.findHttpStatus(error.getErrorCode()));
		}
		
		return new ResponseEntity(response.getRecords(), HttpStatus.OK);
	}
	
	@DeleteMapping(path = "/{id}")
	public ResponseEntity remove(@PathVariable("id") Long brandId) {
		
		ResponseBodyDTO<?> response = this.brandService.remove(brandId);
		
		if (!response.isSucess()) {
			ErrorDTO error = response.getErrors().get(0);
			return new ResponseEntity(response, ErrorCodeEnum.findHttpStatus(error.getErrorCode()));
		}
		
		return new ResponseEntity(HttpStatus.NO_CONTENT);
	}

}
