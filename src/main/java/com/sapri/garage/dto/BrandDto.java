package com.sapri.garage.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@JsonPropertyOrder({"id", "extId", "description"})
public class BrandDto {

	@JsonProperty("id")
	private Long id;
	
	@JsonProperty("extId")
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private String extId;
	
	@JsonProperty("description")
	private String description;
	
}
